<?php

// thx https://github.com/felippe-regazio/php-honeypot-example/blob/master/index.php !
function honeypot_validade ($req) {
    if (!empty($req)) {
        if (isset($req["email"]) && !empty($req["email"])) {
            header('Location:/');
        }
    }
}

// it's likely not a spam so continue:
require_once("env.php");

$req = $db->prepare('SELECT * FROM blogroll_links WHERE link = ?');
$req->execute([htmlspecialchars($_REQUEST["url"])]);
$url = $req->fetch();

if($url && sizeof($url) and !isset($_GET['remove'])){
    ?>
    This url is already on the database.
    <?php
    exit(1);
}

$array_of_reserved_strings = [
    "addfriend",
    "home",
    "browse",
    "search",
    "music",
    "favorites",
    "soon",
    "about",
    "invite",
    "mail",
    "forum",
    "groups",
    "group",
    "events",
    "event",
    "videos",
    "video",
    "help",
    "faq",
    "credits",
    "tos",
    "privacy",
    "imprint",
    "ad",
    "shop",
    "edit",
    "editstatus",
    "editlinks",
    "editphoto",
    "settings",
    "comments",
    "friends",
    "inbox",
    "mail",
    "admin",
    "addfavorite",
    "block",
    "report",
    "comments",
    "addcomment",
    "friends",
    "user",
    "requestemailverification",
    "enable2fa",
    "reset",
    "advent",
    "rules",
    "news"
];

foreach ($array_of_reserved_strings as $reserved_string){
    if(preg_match('/https:\/\/spacehey\.com\/'. $reserved_string .'.?/', htmlspecialchars($_REQUEST["url"]))){
        ?>
        Nope.
        <?php
        exit(1);
    }
}


if(!preg_match('/https:\/\/spacehey\.com\/.+/', htmlspecialchars($_REQUEST["url"]))){
    ?>
    Url pattern does not match with your url, please enter something like https://spacehey.com/sodimel.
    <?php
    exit(1);
}

// check presence of string in profile
$html = file_get_contents(htmlspecialchars($_REQUEST["url"]), false, stream_context_create([
    'http' => [
        'method' => 'GET',
        'header'  => "Cookie: spacehey_user_notice=agreed",
    ]
]));


$profile = substr($html,stripos($html,'<div class="blurbs">')+20); // get blurbs to end
$profile = substr($profile,0,strripos($profile,'<div class="friends">')); // remove friends comments code

if (isset($_GET['remove'])){
    if(!preg_match("/<span style=\"visibility: hidden;\">hello please remove me from the spacehey's unofficial random blogroll, thanks<\/span>/", $profile)){
        ?>
        Error: you didn't add the remove code in your Blurbs section on your profile!
        <?php
        exit(1);
    }

    $req = $db->prepare('DELETE from blogroll_links WHERE link = ?');
    $req->execute([htmlspecialchars($_REQUEST["url"])]);
    $url = $req->fetch();

    ?>
    Your url was successfully removed! You may now remove the invisible code from your Blurbs section, and maybe remove the link/image on your profile?
    <?php
    exit(1);
}
else{
    if(!preg_match("/<span style=\"visibility: hidden;\">hello please add me in the spacehey's unofficial random blogroll, thanks<\/span>/", $profile)){
        ?>
        Error: you didn't add the add code in your Blurbs section on your profile!
        <?php
        exit(1);
    }

    $req = $db->prepare('INSERT into blogroll_links (link) VALUES (?)');
    $req->execute([htmlspecialchars($_REQUEST["url"])]);
    $url = $req->fetch();

    ?>
    Your url was successfully submited! You may now remove the invisible code from your Blurbs section, and paste the link/image on your profile :)
    <?php
    exit(1);
}
