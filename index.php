<?php
require_once("env.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Spacehey unofficial blogroll</title>
        <meta name="description" content="Spacehey unofficial blogroll home page. Join the blogroll list here!" />
    	<meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="<?php echo(url); ?>/design.css" />
    </head>
    <body>
        <h1>Welcome to the Spacehey's unofficial random blogroll!</h1>

        <h2>
            Blogwhat?
        </h2>

        <p>
            This blogroll redirect to a random profile from it's database each time someone click on this link : <?php echo(url); ?>/random.
        </p>

<?php
$res = $db->query("SELECT COUNT(*) FROM blogroll_links");
$count = $res->fetchColumn();
?>
        <h2>
            How to join the other <?php echo $count; ?> members:
        </h2>
        <p>
            This blogroll is rather classical :
        </p>
        <form action="/add.php" method="POST">
        <ul>
            <li>Add this code on your Blurbs section on your profile (it will not be visible):<br />
                <pre style="white-space: pre-wrap;"><code>&lt;span style="visibility: hidden;"&gt;hello please add me in the spacehey's unofficial random blogroll, thanks&lt;/span&gt;</code></pre>
            </li>
            <li>Enter your profile url in this input: <input type="text" name="url" autocomplete="off" placeholder="https://spacehey.com/my_super_url" style="display: inline-block; min-width: 250px;" /></li>
            <li><input type="submit" value="Click this button" /></li>
            <li>Your url is added to the database</li>
            <li>Add the link to your profile (or not ¯\_(ツ)_/¯)</li>
            <li>When someone click the magical link the script will take a random url in the database</li>
            <li>Maybe your profile?</li>
        </ul>
        <!-- this is not for humans, please do not fill this field -->
        <label class="ohno" for="email"></label>
        <input class="ohno" autocomplete="off" type="email" id="email" name="email" placeholder="Your e-mail here" />
        </form>
        <p>
            <i>Total random (you can't pay me to be choosen more than the others), no data scraping (this page is hosted on my own little server on my home in France, I do not collect any data), no bullshit.</i>
        </p>

        <h2>Add the image/link!</h2>

        <ul>
            <li>
                Add this code to your profile:<br />
                <pre style="white-space: pre-wrap;">
<code>&lt;style&gt;/* from https://jdan.github.io/98.css/ by https://spacehey.com/jordan */
    .unofficial-blogroll-link{display: inline-flex; border: 0; background: silver; box-shadow: inset -1px -1px #0a0a0a,inset 1px 1px #fff,inset -2px -2px grey,inset 2px 2px #dfdfdf; box-sizing: border-box; border: none; border-radius: 0; min-width: 75px; min-height: 23px; padding: 0 12px; font-family: Arial; font-size: 11px; color: #222; text-decoration: none;align-items: center;}
    .unofficial-blogroll-link:focus{outline: 1px dotted #000;outline-offset: -4px;}
    .unofficial-blogroll-link:active{box-shadow: inset -1px -1px #fff,inset 1px 1px #0a0a0a,inset -2px -2px #dfdfdf,inset 2px 2px grey;padding: 2px 11px 2px 13px;}
    .unofficial-blogroll-border{display: inline-block; padding: 15px; margin: 15px; background-color: #c0c0c0; box-shadow: inset -1px -1px #fff,inset -2px 1px grey,inset 1px -2px grey,inset 2px 2px #fff;}
    .unofficial-blogroll-about-link{display: inline-block; margin-left: 15px;color: #333;text-shadow: 1px 1px 0 #fff; font-size: 9px;}
    .unofficial-blogroll-about-link:hover{color: black;}
&lt;/style&gt;
&lt;div class="unofficial-blogroll-border"&gt;
    &lt;a href="<?php echo(url); ?>/random" target="_blank" class="unofficial-blogroll-link"&gt;Next profile&lt;/a&gt;
    &lt;a href="<?php echo(url); ?>" target="_blank" class="unofficial-blogroll-about-link"&gt;About&lt;/a&gt;
&lt;/div&gt;</code></pre>
            </li>
            <li>
                Will render to:<br />
                <style>/* from https://jdan.github.io/98.css/ by https://spacehey.com/jordan */
                    .unofficial-blogroll-link{display: inline-flex; border: 0; background: silver; box-shadow: inset -1px -1px #0a0a0a,inset 1px 1px #fff,inset -2px -2px grey,inset 2px 2px #dfdfdf; box-sizing: border-box; border: none; border-radius: 0; min-width: 75px; min-height: 23px; padding: 0 12px; font-family: Arial; font-size: 11px; color: #222; text-decoration: none;align-items: center;}
                    .unofficial-blogroll-link:focus{outline: 1px dotted #000;outline-offset: -4px;}
                    .unofficial-blogroll-link:active{box-shadow: inset -1px -1px #fff,inset 1px 1px #0a0a0a,inset -2px -2px #dfdfdf,inset 2px 2px grey;padding: 2px 11px 2px 13px;}
                    .unofficial-blogroll-border{display: inline-block; padding: 15px; margin: 15px; background-color: #c0c0c0; box-shadow: inset -1px -1px #fff,inset -2px 1px grey,inset 1px -2px grey,inset 2px 2px #fff;}
                    .unofficial-blogroll-about-link{display: inline-block; margin-left: 15px;color: #333;text-shadow: 1px 1px 0 #fff; font-size: 9px;}
                    .unofficial-blogroll-about-link:hover{color: black;}
                </style>
                <div class="unofficial-blogroll-border">
                    <a href="<?php echo(url); ?>/random" target="_blank" class="unofficial-blogroll-link">Next profile</a>
                    <a href="<?php echo(url); ?>" target="_blank" class="unofficial-blogroll-about-link">About</a>
                </div>
            </li>
        </ul>

        <h2>
            Wanna stop appearing on the blogroll?
        </h2>
        <form action="/add.php?remove" method="POST">
            <ul>
                <li>Remove the code to add yourself in the blogroll, and add this code on your Blurbs section on your profile (it will not be visible):<br />
                    <pre style="white-space: pre-wrap;"><code>&lt;span style="visibility: hidden;"&gt;hello please remove me from the spacehey's unofficial random blogroll, thanks&lt;/span&gt;</code></pre>
                </li>
                <li>Add your url in this input: <input type="text" name="url" autocomplete="off" placeholder="https://spacehey.com/my_super_url" style="display: inline-block; min-width: 250px;" /></li>
                <li><input type="submit" value="And click this button" /></li>
            </ul>
            <!-- this is not for humans, please do not fill this field -->
            <label class="ohno" for="email"></label>
            <input class="ohno" autocomplete="off" type="email" id="email" name="email" placeholder="Your e-mail here" />
        </form>

        <hr />

        <h2>
            Answers to FAQ
        </h2>

        <ul>
            <li>
                Beware! If you change your url my database will still redirect to your old url!<br />Here are the two options:
                <ul>
                    <li>
                        Send me a pm on the <a href="https://im.spacehey.com/?user=2163" target="_blank">instant messenger</a> and I will change your url in the database, or
                    </li>
                    <li>
                        Remove yourself from the blogroll, change your url, then register your profile url again.
                    </li>
                </ul>
            </li>
	    <li>
	        Please do not register your profile if your profile is private (or unregister it before going private :)).
    	    </li>
            <li>
                The server is an old Dell Optiplex fx160. I choose this computer from <a href="http://thesizzlewo.webflow.io/blog/get-a-dell-optiplex-fx160-instead-of-a-raspberry-pi" target="_blank">this article</a> :)<br />
                I bought it in 2017 for 50€, and it still runs smoothly :)<br />
                Here's an old <a href="https://l3m.in/img/serveur_petit.png">picture</a> of it.
            </li>
            <li>Please forgive my english, I'm French.</li>
        </ul>

        <hr />

        <h2>
		Changelog
	</h2

	<ul>
		<li>02/09/23: removed stale links</li>
	</ul>

        <hr />

        <p style="text-align:center;font-size: 10px;">A project by <a href="https://spacehey.com/sodimel">corentin</a>, <a href="https://gitlab.com/sodimel/spacehey-unofficial-blogroll">fork it on gitlab</a>!.</p>
    </body>
</html>
